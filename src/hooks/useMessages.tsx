import { useCallback, useContext, useEffect, useMemo, useRef } from 'react';
import { Message, observable, Priority } from '../Api';
import { MessagesContext } from '../context/MessagesContext';

export default function useMessages() {
  const {
    messages,
    setMessages,
    errorMessages,
    setErrorMessages,
    subscription,
  } = useContext(MessagesContext);

  const currentErrorMessage = useRef('');

  const warnMessages = useMemo(
    () => messages?.filter(message => message.priority === Priority.Warn),
    [messages]
  );

  const infoMessages = useMemo(
    () => messages?.filter(message => message.priority === Priority.Info),
    [messages]
  );

  const onClearErrorMessage = useCallback(
    (errorMessage: string) => {
      currentErrorMessage.current = '';
      setErrorMessages(prevErrorMessages =>
        prevErrorMessages.filter(
          prevErrorMessage => prevErrorMessage !== errorMessage
        )
      );
    },
    [setErrorMessages]
  );

  const onClearMessage = useCallback(
    (message: Message) => {
      setMessages(prevMessages =>
        prevMessages.filter(
          prevMessage => prevMessage.message !== message.message
        )
      );
    },
    [setMessages]
  );

  const generateMessage = useCallback(
    (message: Message) => {
      if (message.priority === Priority.Error) {
        setErrorMessages(oldErrorMessages => {
          const newErrorMessages = [...oldErrorMessages];
          newErrorMessages.unshift(message.message);
          currentErrorMessage.current = message.message;
          return newErrorMessages;
        });
      } else {
        setMessages(oldMessages => {
          const newMessages = [...oldMessages];
          newMessages.unshift(message);
          return newMessages;
        });
      }
    },
    [setErrorMessages, setMessages]
  );

  const startSubscription = useCallback(() => {
    return observable.subscribe({
      next: generateMessage,
    });
  }, [generateMessage]);

  useEffect(() => {
    subscription.current = startSubscription();

    return () => subscription.current?.unsubscribe();
  }, [setMessages, setErrorMessages, subscription, startSubscription]);

  return {
    errorMessages,
    warnMessages,
    infoMessages,
    onClearErrorMessage,
    onClearMessage,
    currentErrorMessage: currentErrorMessage.current,
    startSubscription,
  };
}
