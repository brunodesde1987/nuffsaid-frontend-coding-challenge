import React, {
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useState,
} from 'react';
import MessageCard from './components/Card';
import styled from 'styled-components';
import ActionButton from './components/Button';
import { Box } from '@material-ui/core';
import Columns from './components/Columns';
import Column from './components/Column';
import Heading from './components/Heading';
import useMessages from './hooks/useMessages';
import { MessagesContext } from './context/MessagesContext';
import ErrorNotification from './components/ErrorNotification';

const StyledHeader = styled.header`
  font-size: 24px;
`;

const StyledBox = styled(Box)`
  gap: 4px;
`;

const App: React.FC<{}> = () => {
  const {
    errorMessages,
    warnMessages,
    infoMessages,
    onClearErrorMessage,
    onClearMessage,
    currentErrorMessage,
    startSubscription,
  } = useMessages();
  const { setErrorMessages, setMessages, subscription } =
    useContext(MessagesContext);
  const [openErrorNotification, setOpenErrorNotification] = useState(false);
  const [, setSubscriptionStarted] = useState(true);

  useEffect(() => {
    if (errorMessages.length > 0 && currentErrorMessage) {
      setOpenErrorNotification(true);
    }
  }, [errorMessages.length, currentErrorMessage]);

  const handleClearMessages = useCallback(() => {
    setErrorMessages([]);
    setMessages([]);
  }, [setErrorMessages, setMessages]);

  const handleCloseNotification = useCallback(
    () => setOpenErrorNotification(false),
    [setOpenErrorNotification]
  );

  const handleToggleSubscription = useCallback(() => {
    setSubscriptionStarted(prevSubscriptionStarted => {
      prevSubscriptionStarted
        ? subscription.current?.unsubscribe()
        : (subscription.current = startSubscription());

      return !prevSubscriptionStarted;
    });
  }, [setSubscriptionStarted, startSubscription, subscription]);

  const MemoizedErrorMessagesCards = useMemo(() => {
    return errorMessages?.map?.(errorMessage => (
      <MessageCard
        key={errorMessage}
        kind="error"
        onClear={() => onClearErrorMessage(errorMessage)}
      >
        {errorMessage}
      </MessageCard>
    ));
  }, [errorMessages, onClearErrorMessage]);

  const MemoizedWarnMessagesCards = useMemo(() => {
    return warnMessages?.map?.(warnMessage => (
      <MessageCard
        key={warnMessage?.message}
        kind="warn"
        onClear={() => onClearMessage(warnMessage)}
      >
        {warnMessage?.message}
      </MessageCard>
    ));
  }, [warnMessages, onClearMessage]);

  const MemoizedInfoMessagesCards = useMemo(() => {
    return infoMessages?.map?.(infoMessage => (
      <MessageCard
        key={infoMessage?.message}
        kind="info"
        onClear={() => onClearMessage(infoMessage)}
      >
        {infoMessage?.message}
      </MessageCard>
    ));
  }, [infoMessages, onClearMessage]);

  return (
    <>
      {openErrorNotification && (
        <ErrorNotification
          onClose={handleCloseNotification}
          message={currentErrorMessage}
        />
      )}
      <StyledHeader>nuffsaid.com Coding Challenge</StyledHeader>
      <hr />
      <StyledBox
        display="flex"
        alignItems="center"
        justifyContent="center"
        marginBottom="64px"
      >
        <ActionButton onClick={handleToggleSubscription}>
          {subscription.current?.closed ? 'Start' : 'Stop'}
        </ActionButton>
        <ActionButton onClick={handleClearMessages}>Clear</ActionButton>
      </StyledBox>
      <Columns>
        <Column>
          <Heading>Error Type 1</Heading>
          <span>Count {errorMessages.length}</span>
          {MemoizedErrorMessagesCards}
        </Column>
        <Column>
          <Heading>Warning Type 2</Heading>
          <span>Count {warnMessages.length}</span>
          {MemoizedWarnMessagesCards}
        </Column>
        <Column>
          <Heading>Info Type 3</Heading>
          <span>Count {infoMessages.length}</span>
          {MemoizedInfoMessagesCards}
        </Column>
      </Columns>
    </>
  );
};

export default App;
