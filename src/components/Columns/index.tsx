import React from 'react';
import Box from '@material-ui/core/Box';

export default function Columns({ children }: { children: React.ReactNode }) {
  return (
    <Box display="flex" justifyContent="center" gridGap="16px">
      {children}
    </Box>
  );
}
