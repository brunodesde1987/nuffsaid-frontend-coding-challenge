import { render } from '@testing-library/react';
import Columns from './index';

describe('Columns', () => {
  it('renders the Columns', async () => {
    const { container } = render(
      <Columns>
        <div>Column 1</div>
        <div>Column 2</div>
      </Columns>
    );

    expect(container.firstChild).toBeInTheDocument();
    expect(container.firstChild).toBeVisible();
    expect(container.firstChild?.childNodes).toHaveLength(2);
  });
});
