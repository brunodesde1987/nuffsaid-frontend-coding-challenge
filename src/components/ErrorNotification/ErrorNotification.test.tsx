import { render, screen, waitFor } from '@testing-library/react';
import ErrorNotification from './index';

const handleClose = jest.fn();

describe('ErrorNotification', () => {
  it('renders the ErrorNotification', async () => {
    render(
      <ErrorNotification onClose={handleClose} message={'some error =('} />
    );

    const errorNotification = await screen.findByText(/some error =\(/i);
    expect(errorNotification).toBeInTheDocument();
    expect(errorNotification).toBeVisible();
    await waitFor(() => expect(handleClose).toHaveBeenCalledTimes(1), {
      timeout: 2000,
    });
  });

  it('should call onClose after 2 seconds', async () => {
    render(
      <ErrorNotification onClose={handleClose} message={'some error =('} />
    );

    await waitFor(() => expect(handleClose).toHaveBeenCalledTimes(1), {
      timeout: 3000,
    });
  });
});
