import React from 'react';
import Snackbar, { SnackbarProps } from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';

function ErrorNotification({ onClose, message }: SnackbarProps) {
  return (
    <Snackbar
      open={true}
      anchorOrigin={{
        vertical: 'top',
        horizontal: 'right',
      }}
      autoHideDuration={2000}
      onClose={onClose}
    >
      <MuiAlert elevation={6} variant="filled" severity="error">
        {message}
      </MuiAlert>
    </Snackbar>
  );
}

export default React.memo(ErrorNotification);
