import userEvent from '@testing-library/user-event';
import { render, screen, waitFor } from '@testing-library/react';
import MessageCard from './index';

const handleClear = jest.fn();

describe('MessageCard', () => {
  it('renders the MessageCard', async () => {
    const { container } = render(
      <MessageCard kind="info" onClear={handleClear}>
        Info
      </MessageCard>
    );

    expect(container.firstChild).toBeInTheDocument();
    expect(container.firstChild).toBeVisible();
  });

  it('renders the MessageCard with green background when has info kind', async () => {
    render(
      <MessageCard kind="info" onClear={handleClear}>
        Info
      </MessageCard>
    );

    const messageCard = await screen.findByText(/info/i);
    expect(messageCard.parentElement).toHaveClass('info');
  });

  it('renders the MessageCard with yellow background when has warn kind', async () => {
    render(
      <MessageCard kind="warn" onClear={handleClear}>
        Warn
      </MessageCard>
    );

    const messageCard = await screen.findByText(/warn/i);
    expect(messageCard.parentElement).toHaveClass('warn');
  });

  it('renders the MessageCard with orange background when has error kind', async () => {
    render(
      <MessageCard kind="error" onClear={handleClear}>
        Error
      </MessageCard>
    );

    const messageCard = await screen.findByText(/error/i);
    expect(messageCard.parentElement).toHaveClass('error');
  });

  it('should call onClear when clicks on the Clear card action', async () => {
    render(
      <MessageCard kind="info" onClear={handleClear}>
        Info
      </MessageCard>
    );

    const clearCardAction = await screen.findByText(/clear/i);

    userEvent.click(clearCardAction);
    expect(clearCardAction).toHaveClass('clear');
    await waitFor(() => expect(handleClear).toHaveBeenCalledTimes(1));
  });
});
