import React from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import styled from 'styled-components';

interface MessageCardProps {
  children: React.ReactNode;
  kind: 'error' | 'warn' | 'info';
  onClear: React.MouseEventHandler<HTMLSpanElement>;
}

const StyledCard = styled(Card)`
  width: 400px;
  margin: 10px 0;

  &.error {
    background-color: #f56236;
  }

  &.warn {
    background-color: #fce788;
  }

  &.info {
    background-color: #88fca3;
  }
`;

const StyledCardContent = styled(CardContent)`
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`;

const StyledCardActions = styled(CardActions)`
  font-family: 'Roboto';
  font-size: 0.9rem;
  justify-content: flex-end;
  padding: 0 20px 16px;

  .clear {
    cursor: pointer;

    &:hover {
      opacity: 0.8;
    }
  }
`;

function MessageCard({ children, kind, onClear }: MessageCardProps) {
  return (
    <StyledCard className={kind}>
      <StyledCardContent title={String(children)}>{children}</StyledCardContent>
      <StyledCardActions>
        <span className="clear" title="Remove this message" onClick={onClear}>
          Clear
        </span>
      </StyledCardActions>
    </StyledCard>
  );
}

export default React.memo(MessageCard);
