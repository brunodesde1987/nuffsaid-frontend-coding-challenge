import React from 'react';
import styled from 'styled-components';
import { StylesProvider } from '@material-ui/core/styles';
import Button, { ButtonProps } from '@material-ui/core/Button';

const StyledButton = styled(Button)`
  font-weight: 600;
  text-transform: uppercase;
  background-color: #89fba6;
  padding: 3px 16px;
`;

function ActionButton({ children, onClick, ...props }: ButtonProps) {
  return (
    <StylesProvider injectFirst>
      <StyledButton
        variant="contained"
        size="small"
        onClick={onClick}
        {...props}
      >
        {children}
      </StyledButton>
    </StylesProvider>
  );
}

export default React.memo(ActionButton);
