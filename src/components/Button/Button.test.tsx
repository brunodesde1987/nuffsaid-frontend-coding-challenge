import userEvent from '@testing-library/user-event';
import { render, screen, waitFor } from '@testing-library/react';
import ActionButton from './index';

describe('ActionButton', () => {
  it('renders the ActionButton', async () => {
    render(<ActionButton>Stop</ActionButton>);

    const actionButton = await screen.findByRole('button', {
      name: /stop/i,
    });

    expect(actionButton).toBeInTheDocument();
    expect(actionButton).toBeVisible();
    expect(actionButton).toBeEnabled();
  });

  it('should call handleClick when clicked', async () => {
    const handleClick = jest.fn();
    render(<ActionButton onClick={handleClick}>Stop</ActionButton>);

    const actionButton = await screen.findByRole('button', {
      name: /stop/i,
    });

    userEvent.click(actionButton);

    await waitFor(() => expect(handleClick).toHaveBeenCalledTimes(1));
  });
});
