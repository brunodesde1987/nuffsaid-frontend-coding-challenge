import { render, screen } from '@testing-library/react';
import Heading from './index';

describe('Heading', () => {
  it('renders the Heading', async () => {
    render(<Heading>nuffsaid</Heading>);

    const heading = await screen.findByRole('heading', {
      name: /nuffsaid/i,
    });

    expect(heading).toBeInTheDocument();
    expect(heading).toBeVisible();
    expect(heading.tagName).toEqual('H6');
  });
});
