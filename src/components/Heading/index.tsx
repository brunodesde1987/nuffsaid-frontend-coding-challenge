import React from 'react';
import styled from 'styled-components';
import { StylesProvider } from '@material-ui/core/styles';
import Typography, { TypographyProps } from '@material-ui/core/Typography';

const StyledTypography = styled(Typography)`
  font-family: 'Times New Roman', Times, serif;
  font-weight: bold;
  font-size: 1.5rem;
  letter-spacing: -1px;
  line-height: 1.2;
`;

export default function Heading({ children, ...props }: TypographyProps) {
  return (
    <StylesProvider injectFirst>
      <StyledTypography variant="h6" {...props}>
        {children}
      </StyledTypography>
    </StylesProvider>
  );
}
