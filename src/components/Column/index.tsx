import React from 'react';
import styled from 'styled-components';

const StyledDiv = styled.div`
  min-width: 400px;
`;

export default function Column({ children }: { children: React.ReactNode }) {
  return <StyledDiv>{children}</StyledDiv>;
}
