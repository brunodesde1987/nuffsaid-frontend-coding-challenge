import { render } from '@testing-library/react';
import Column from './index';

describe('Column', () => {
  it('renders the Column', async () => {
    const { container } = render(
      <Column>
        <span>Content</span>
      </Column>
    );

    expect(container.firstChild).toBeInTheDocument();
    expect(container.firstChild).toBeVisible();
    expect(container.firstChild?.childNodes).toHaveLength(1);
  });
});
