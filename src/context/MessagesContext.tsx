import React, { useRef, useState } from 'react';
import { Subscription } from 'rxjs';
import { Message } from '../Api';

interface IMessagesContext {
  errorMessages: string[];
  setErrorMessages: React.Dispatch<React.SetStateAction<string[]>>;
  messages: Message[];
  setMessages: React.Dispatch<React.SetStateAction<Message[]>>;
  subscription: React.MutableRefObject<Subscription | undefined>;
}

export const MessagesContext = React.createContext<IMessagesContext>({
  errorMessages: [],
  setErrorMessages: () => [],
  messages: [],
  setMessages: () => [],
  subscription: { current: undefined },
});

export default function MessagesProvider({
  children,
}: {
  children: React.ReactNode;
}) {
  const [messages, setMessages] = useState<Message[]>([]);
  const [errorMessages, setErrorMessages] = useState<string[]>([]);
  const subscription = useRef<Subscription>();

  return (
    <MessagesContext.Provider
      value={{
        messages,
        setMessages,
        errorMessages,
        setErrorMessages,
        subscription,
      }}
    >
      {children}
    </MessagesContext.Provider>
  );
}
